package com.slalom.eda.kafka.consumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;

@SpringBootApplication
public class ConsumerApplication {

	public static final String KAFKA_TOPIC = "simpleloop";
	public static Logger logger = LoggerFactory.getLogger(ConsumerApplication.class);

	@Autowired
	private KafkaTemplate<String, String> template;

	public static void main(String[] args) {
		SpringApplication.run(ConsumerApplication.class, args);
	}

	@KafkaListener(topics = KAFKA_TOPIC)
	public void listen(ConsumerRecord<?, ?> cr) throws Exception {
		logger.info(cr.toString());
	}
}
